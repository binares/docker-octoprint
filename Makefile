DEV_CONTAINER=devoctoprint
DEV_IMAGE=myoctoprint
build:
	docker build --tag ${DEV_IMAGE} .

run:
	docker run -p 80:80 -d --name $DEV_CONTAINER ${DEV_IMAGE}:latest

stop:
	docker stop $[DEV_CONTAINER}

rm: stop
	docker rm ${DEV_CONTAINER}

clean: stop rm

attach:
	docker exec -it ${DEV_CONTAINER} bash 

rebuild: stop rm build run attach 

extract-config:
	docker cp ${DEV_CONTAINER}:/octoprint/config.yaml ./config.yaml
