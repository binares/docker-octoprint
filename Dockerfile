FROM octoprint/octoprint

ENV PIP_USER false
RUN pip install "https://github.com/j7126/OctoPrint-Dashboard/archive/master.zip"
RUN pip install "https://github.com/imrahil/OctoPrint-NavbarTemp/archive/master.zip"
RUN pip install "https://github.com/OllisGit/OctoPrint-DisplayLayerProgress/releases/latest/download/master.zip"
RUN pip install "https://github.com/OctoPrint/OctoPrint-MQTT/archive/master.zip"
RUN pip install "https://github.com/eyal0/OctoPrint-PrintTimeGenius/archive/master.zip"
RUN pip install "https://github.com/OllisGit/OctoPrint-FilamentManager/releases/latest/download/master.zip"
RUN pip install "https://github.com/OllisGit/OctoPrint-PrintJobHistory/releases/latest/download/master.zip"
RUN pip install "https://github.com/borisbu/OctoRelay/archive/master.zip"
RUN pip install "https://github.com/birkbjo/OctoPrint-Themeify/archive/master.zip"
RUN pip install "https://github.com/jneilliii/OctoPrint-BedLevelVisualizer/archive/master.zip"
RUN pip install "https://github.com/jneilliii/OctoPrint-MQTTPublish/archive/master.zip"
RUN pip install "https://github.com/jneilliii/OctoPrint-PrusaSlicerThumbnails/archive/master.zip"
RUN pip install "https://github.com/OctoPrint/OctoPrint-FirmwareUpdater/archive/master.zip"


RUN echo "dummy"
# WORKDIR /addons
# RUN git clone https://github.com/adafruit/Adafruit_Python_DHT.git
# WORKDIR /addons/Adafruit_Python_DHT
# RUN apt-get install -y build-essential python-dev python-openssl
# RUN python setup.py install --force-pi
# ENV PIP_USER true
